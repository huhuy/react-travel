import React from 'react'
import styles from './SideMenu.module.css'
import {sideMenuList} from "./mockup";
import {Menu} from 'antd'
import {GifOutlined} from '@ant-design/icons'

export const SideMenu: React.FC = () => {
    return (
        <Menu mode="vertical" className={styles["side-menu"]}>
            {sideMenuList.map((m, index) => (
                <Menu.SubMenu
                    key={`side-menu${index}`}
                    title={<span><GifOutlined style={{marginRight: 10}}/>{m.title}</span>}
                >
                    {m.subMenu.map((sm, smIndex) => (
                        <Menu.SubMenu
                            key={`sub-menu${index}-item${smIndex}`}
                            title={<span><GifOutlined style={{marginRight: 10}}/>{sm.title}</span>}
                        >
                            {sm.subMenu.map((sms, smsIndex) => (
                                <Menu.Item
                                    key={`sub-menu${index}-item${smIndex}-item${smsIndex}`}>
                                    <span><GifOutlined style={{marginRight: 10}} />{sms}</span>
                                </Menu.Item>
                            ))}
                        </Menu.SubMenu>
                    ))}
                </Menu.SubMenu>
            ))}
        </Menu>
    );
};