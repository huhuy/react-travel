import React from 'react';
import styles from './Carousel.module.css';
import {Image, Carousel as AntdCarousel} from 'antd';

import CarouselImage1 from '../../assets/images/carousel_1.jpg';
import CarouselImage2 from '../../assets/images/carousel_2.jpg';
import CarouselImage3 from '../../assets/images/carousel_3.jpg'

export const Carousel: React.FC = () => {
    return (
        <AntdCarousel autoplay className={styles.slider}>
            <Image src={CarouselImage1}/>
            <Image src={CarouselImage2}/>
            <Image src={CarouselImage3}/>
        </AntdCarousel>
    )
};
