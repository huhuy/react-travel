import React from 'react';
import {Image, Typography} from 'antd'
import {useHistory} from 'react-router-dom'

interface propsType {
    id: number | string;
    title: string;
    price: number | string;
    imageSrc: string;
    size: 'large' | 'small'
}

export const ProductImage: React.FC<propsType> = ({id, title, size, imageSrc, price}) => {
    const history = useHistory();
    return (
        <div onClick={() => history.push(`detail/${id}`)}>
            {size === 'large' ?
                (<Image src={imageSrc} height={290} width={487}/>)
                : (<Image src={imageSrc} height={120} width={240}/>)
            }
            <div>
                <Typography.Text type="secondary"> {title.slice(0, 25)}</Typography.Text>
                <Typography.Text type="danger" strong>￥ {price} 起</Typography.Text>
            </div>
        </div>
    )
};