import React from 'react'
import sideImage1 from '../../assets/images/sider_2019_12-09.png';
import sideImage2 from '../../assets/images/sider_2019_02-04.png';
import sideImage3 from '../../assets/images/sider_2019_02-04-2.png';
import {productList1, productList2, productList3} from "./mockup";
import {ProductCollection} from './ProductCollection'
import {Typography} from 'antd'

export const Product: React.FC = () => {
    return (
        <>
            <ProductCollection
                title={
                    <Typography.Title level={3} type="warning">
                        爆款推荐
                    </Typography.Title>}
                sideImage={sideImage1}
                products={productList1}
            />
            <ProductCollection
                title={
                    <Typography.Title level={3} type="danger">
                        新品上市
                    </Typography.Title>}
                sideImage={sideImage2}
                products={productList2}
            />
            <ProductCollection
                title={
                    <Typography.Title level={3} type="success">
                        国内游推荐
                    </Typography.Title>}
                sideImage={sideImage3}
                products={productList3}
            />
        </>

    )
};