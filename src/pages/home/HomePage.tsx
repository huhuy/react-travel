import React from 'react'
import styles from "./HomePage.module.css";
import {Header, Footer, SideMenu, Carousel, Product, BusinessPartners} from '../../components';
import {Row, Col} from 'antd'

export const HomePage:React.FC = () => {
    return(
        <>
            <Header/>
            {/* 页面内容 content */}
            <div className={styles["page-content"]}>
                <Row style={{marginTop: 20}}>
                    <Col span={6}>
                        <SideMenu/>
                    </Col>
                    <Col span={18}>
                        <Carousel/>
                    </Col>
                </Row>
                {/*推荐*/}
                <Product/>
                {/*合作企业*/}
                <BusinessPartners/>
            </div>
            <Footer/>
        </>
    )
};