import React from "react";
import {RouteComponentProps} from 'react-router-dom'
// import styles from './DetailPage.module.css'

interface MatchParams {
    touristRouteId: string
}

export const DetailPage: React.FC<RouteComponentProps<MatchParams>> = (props) => {

    console.log(props);
    return (
        <div>
            <h1>路游路线详情页面, 路线ID: {props.match.params.touristRouteId}</h1>
        </div>
    )
};
